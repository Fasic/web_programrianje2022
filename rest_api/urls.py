from django.urls import path
from rest_api import views

app_name = 'rest_api'
urlpatterns = [
    path('category/', views.CategoryListView.as_view(), name='cat_list'),
    path('category/<int:pk>', views.CategoryDetailsView.as_view(), name='cat_details'),
]