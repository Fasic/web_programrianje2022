from rest_framework import routers, serializers, viewsets

from todos.models import Category


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = "__all__"
