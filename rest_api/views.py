from django.shortcuts import render

# Create your views here.
from rest_framework import generics
from rest_api.serializers import CategorySerializer
from todos.models import Category


class CategoryDetailsView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer


class CategoryListView(generics.ListCreateAPIView):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer



