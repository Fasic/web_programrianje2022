from django.http import HttpResponse, Http404, HttpResponseRedirect
from django.shortcuts import render

# Create your views here.
from django.urls import reverse
from django.views import View

from aplikacija1.forms import FormStudent
from aplikacija1.models import Student


def index(request):
    return render(request, 'home.html', {})


def stranica1(request):
    return render(request, 'stranica.html', {'poruka': 1})


def stranica2(request):
    return render(request, 'stranica.html', {'poruka': 2})


def studenti(request):
    try:
        s = Student.objects.all()
        return render(request, 'studenti.html', {'studenti': s})
    except:
        raise Http404("Nema studenata")


def student(request, id):
    try:
        s = Student.objects.get(pk=id)
        return render(request, 'student.html', {'student': s})
    except:
        raise Http404("Nema studenta")


def studentDelete(request, id):
    try:
        s = Student.objects.get(pk=id)
        s.delete()
        return HttpResponseRedirect(reverse('aplikacija:studenti'))
    except:
        raise Http404("Nema studenta")


def noviStudent(request):
    if (request.method == 'POST'):
        form = FormStudent(request.POST)
        if (form.is_valid()):
            form.save()
            return HttpResponseRedirect(reverse('aplikacija:studenti'))
        return render(request, 'noviStudent.html', {'form': form})
    else:
        form = FormStudent()
        return render(request, 'noviStudent.html', {'form': form})


class StudentView(View):
    def get(self, request):
        form = FormStudent()
        return render(request, 'noviStudent.html', {'form': form})

    def post(self, request):
        form = FormStudent(request.POST)
        if (form.is_valid()):
            form.save()
            return HttpResponseRedirect(reverse('aplikacija:studenti'))
        return render(request, 'noviStudent.html', {'form': form})
