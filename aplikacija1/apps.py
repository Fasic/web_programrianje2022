from django.apps import AppConfig


class Aplikacija1Config(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'aplikacija1'
