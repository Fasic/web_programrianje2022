from django.db import models


# Create your models here.
class Student(models.Model):
    ime = models.CharField(max_length=50)
    prezime = models.CharField(max_length=50)
    starost = models.IntegerField()

    def __str__(self):
        return self.ime + " " + self.prezime
