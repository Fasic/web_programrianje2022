from django import forms

from aplikacija1.models import Student


class FormStudent(forms.ModelForm):
    #dodatna polja

    class Meta:
        model = Student
        fields = '__all__'
