# APP URLs

from django.urls import path
from aplikacija1 import views

app_name = 'aplikacija'
urlpatterns = [
    path('', views.index, name='index'),

    path('stranica1/', views.stranica1, name='stranica1'),
    path('stranica2/', views.stranica2, name='stranica2'),
    path('add_student/', views.StudentView.as_view(), name='AddStudent'),
    path('student/', views.studenti, name='studenti'),

    path('student/<int:id>', views.student, name='student'),
    path('student/delete/<int:id>', views.studentDelete, name='studentDelete'),
]

# http://127.0.0.1:8000/

# http://127.0.0.1:8000/admin
# http://127.0.0.1:8000/aplikacija

# http://127.0.0.1:8000/aplikacija
# http://127.0.0.1:8000/aplikacija/stranica1
# http://127.0.0.1:8000/aplikacija/stranica2

# http://127.0.0.1:8000/aplikacija/student/1