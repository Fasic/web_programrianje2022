from django.urls import path
from todos import views

app_name = 'todos'
urlpatterns = [
    path('home', views.home, name='home'),
    path('todos', views.Todos.as_view(), name='todos'),

    path('todos/update/<int:pk>', views.TaskUpdateView.as_view(), name='todos_update'),

    path('todos/delete/<int:pk>', views.TaskDeleteView.as_view(), name='delete_todo'),
    path('todos/done/<int:id>', views.done_todo, name='done_todo'),

    path('category/create', views.CategoryCreateView.as_view(), name='category_create'),

]
