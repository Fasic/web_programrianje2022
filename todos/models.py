from django.db import models


class Category(models.Model):
    name = models.CharField(max_length=50)
    description = models.CharField(max_length=500, null=True, blank=True)

    def __str__(self):
        return f"{self.name} ({len(self.description)})"


class Task(models.Model):
    title = models.CharField(max_length=50)
    description = models.CharField(max_length=500)
    done = models.BooleanField(default=False)
    priority = models.IntegerField()
    category = models.ForeignKey(Category, on_delete=models.SET_NULL, null=True)

    def __str__(self):
        return f"{self.title} ({self.priority})"
