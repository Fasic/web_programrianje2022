from django.http import Http404, HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse, reverse_lazy
from django.views import View
from django.views.generic import CreateView, UpdateView, DeleteView

from todos.forms import FormTask
from todos.models import Task, Category


def home(request):
    return render(request, 'todosHome.html', {})


def delete_todo(request, id):
    try:
        t = Task.objects.get(pk=id)
        t.delete()
        return HttpResponseRedirect(reverse('todos:todos'))
    except:
        raise Http404("Nema taska, sa ovim ID-em")


def done_todo(request, id):
    try:
        t = Task.objects.get(pk=id)
        t.done = not t.done
        t.save()
        return HttpResponseRedirect(reverse('todos:todos'))
    except:
        raise Http404("Nema taska, sa ovim ID-em")


class Todos(View):
    def get(self, request):
        tasks = Task.objects.all()
        form = FormTask()
        return render(request, 'todos.html', {'tasks': tasks, 'form': form})

    def post(self, request):
        form = FormTask(request.POST)
        if form.is_valid():
            form.save()

            form = FormTask()
            tasks = Task.objects.all()
            return render(request, 'todos.html', {'tasks': tasks, 'form': form})
        tasks = Task.objects.all()
        return render(request, 'todos.html', {'tasks': tasks, 'form': form})


class CategoryCreateView(CreateView):
    model = Category
    fields = '__all__'
    template_name = 'form.html'
    success_url = reverse_lazy('todos:todos')


class TaskUpdateView(UpdateView):
    model = Task
    fields = '__all__'
    template_name = 'form.html'
    success_url = reverse_lazy('todos:todos')


class TaskDeleteView(DeleteView):
    model = Task
    template_name = 'confirm_delete.html'
    success_url = reverse_lazy('todos:todos')
