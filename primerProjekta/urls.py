# PROJECT URLs

from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),
    path('app/', include('aplikacija1.urls')),
    path('todo/', include('todos.urls')),

    path('api/', include('rest_api.urls')),
]
